# MDE using a GPL: Java

## Introduction

The emergence of software models is inevitably linked emerged over the different generations that have emerged over the years in terms of programming languages.

With technology evolving fast, there is a need to write and maintain software more efficiently, and better communicate with team members. As developers, we rarely get to think about these things as we rush to meet deadlines.

**Model-Driven Engineering (MDE)** is a "software development approach that considers the systematic use of models to develop software rather than using general purpose programming languages".

It is considered a new paradigm in the field of software engineering. It is based on the separation of the system functionality being developed and the implementation of such a system for one specific platform.

The main goal of this document is to show how we can achieve **MDE** using plain **Java**.

Regarding **Java**, it is an object oriented language which gives a clear structure to programs and allows code to be reused and is also open-source and free with a huge community support.
We can find evidence of its usage in :

- Mobile applications (specially Android apps)
- Desktop applications
- Web applications
- Web servers and application servers
- Games
- Database connection
- And many more

Java platform is one of the most used platforms for developing software in a wide range of application domains. It has large number of packages supporting the software development.

## MDE with Java

### Scenario

Java is an implementation language. The natural approach to modelling the scenario is therefore to implement it. However this need doesn't mean a loss of abstraction: we can use features of the language to express abstract concepts without constraining their implementation.
In order to show of how to achieve the principles (at least try to) of **MDE** using a GLP like **Java** we need to describe a major scenario in which we can perform our implementation.
Saying that we agreed the following:

- Create a metamodel the mindmap project using java and create an instance to model the hypothetical Gorgeous Food Application
- Add validations to the model
- Create a DSL using java for the mindmap model
- Create a instance to model the hypothetical Gorgeous Food Application
- Perform Model to Model Transformations
- Perform Model to/from Text Transformations

Is important to note that the Eclipse was used as IDE of choice.

#### Gorgeous Food Context

A very innovative school at Porto has become concerned about the food available for its academic community, especially its students, many of whom are displaced from their relatives homes, have little economic means and poor competence in preparing and cooking healthy meals. The institution no longer has vending machines available offering very poor nutritional quality food. Affordable meals are served in the canteen. It has managed to offer delicious lunches and dinners due to a partnership with a famous hotel school, and the intense collaboration of their students enrolled in the Culinary Arts program.

In addition, the canteen has workers skilled on nutrition science to ensure balanced meals from Monday to Friday. The service has received wide coverage from the national media but also from nearby countries. Photos of the meals served are published in unusual numbers on social networks.
However, there are often unserved meals that currently represent an unacceptable waste of resources. It seems attractive the idea of selling these meals at another price to anyone now that popularity seems to be increasing, which has been turned into a case study.

On the other hand, there are many working students who, in combination with their professional and academic duties, are not always able to attend the canteen during their opening hours. Microwaves in various buildings provide students with some comfort due to the ability to heat and consume previously prepared meals at home, which requires a planning that is not always feasible.
Thus, a new service, Gorgeous Food, was planned and a feasibility study showed its interest for two target groups:

- Students enrolled in the institutions courses,
- Other people, due to the balanced and elegant meals that are not expensive even at a normal price.

Unserved meals are immediately packaged, labelled with meal designation, identification number, production and expiration dates, and then frozen by the kitchen workers. The collaboration with the hotel school was also helpful in this process. These units have their inventory managed by the software to be developed.

The application should also provide for each type of meal a characterization regarding the meal designation ("styled green soup", "Portuguese fish and fries", "stone soup", "Jewish sausage with divine vegetable", "cream of the Porto sky"), its type (soup, main course, dessert, among others), but also nutritional data, as well as its list of ingredients and possible allergens. Meals will be available on campus 24 hours daily and at a specific location. Meals are paid there, but the type of user who purchased them is recorded.

### Metamodels

![Mindmap Metamodel](images/mindmap.png)

The mindmap above is what we are trying to achieve using Java.
First thing first we need to create a project of type Java in our IDE.
Next create 4 classes in java:

- Map
- MapElement (abstract class)
- Relationship
- Topic

After creating the class we need to add some logic as shown in the images bellow for it to work as envisioned.

```
/* For the Map.java */
package mdejava;

import java.util.ArrayList;

public class Map {
	private String title;
	private ArrayList<Topic> rootTopics;

	void addRootTopic(Topic topic) {
		rootTopics.add(topic);
	}

	void removeRootTopic(Topic topic) {
		rootTopics.add(topic);
	}

	public Map (String title) {
		this.title = title;
		this.rootTopics = new ArrayList<Topic>();
	}
}

```

```
/* For the Map Element.java */
package mdejava;

public abstract class MapElement {
	private String name;

	public void setName(String newName) {
        name = newName;
    }

    public String getName() {
        return name;
    }
}

```

```
/* For the Topic.java */
package mdejava;

import java.util.ArrayList;

public class Topic extends MapElement {

	private String description;
	private String start;
	private String end;
	private Priority priority;
	private Topic parent;

	enum Priority {
		HIGH,
		MEDIUM,
		LOW;
	}

	private ArrayList<Topic> allSubTopics;

	void addSubTopic(Topic topic) {
		allSubTopics.add(topic);
	}

	void removeSubTopic(Topic topic) {
		allSubTopics.add(topic);
	}

	public Topic(String name, String description, String start, String end, Priority priority) {

		setName(name);
		this.description = description;
		this.start = start;
		this.end = end;
		this.priority = Priority.HIGH;
		this.allSubTopics = new ArrayList<Topic>();
	}
}

```

```
/* For the Relationship.java */
package mdejava;

public class Relationship extends MapElement {
	enum Type {
		DEPENDENCY,
		INCLUDE,
		EXTEND;
	}

	Type type;
	Topic target;
	Topic source;

	public Relationship(Topic target, Topic source, Type type) {
		this.source = source;
		this.target = target;
		this.type = type.DEPENDENCY;
	}
}

```


### DSL

The basic idea of a Domain-Specific Language (DSL) is a computer language that's targeted to a particular kind of problem, rather than a general purpose language that's aimed at any kind of software problem. Domain-specific languages have been talked about, and used for almost as long as computing has been done.

An important and useful distinction I make is between internal and external DSLs. Internal DSLs are particular ways of using a host language to give the host language the feel of a particular language. This approach has recently been popularized by the Ruby community although it's had a long heritage in other languages - in particular Lisp. Although it's usually easier in low-ceremony languages like that, you can do effective internal DSLs in more mainstream languages like Java and C#.

External DSLs have their own custom syntax and you write a full parser to process them. There is a very strong tradition of doing this in the Unix community. Many XML configurations have ended up as external DSLs, although XML's syntax is badly suited to this purpose.

The most most common DSLs in the wild today are textual, but you can have graphical DSLs too. Graphical DSLs requires a tool along the lines of a Language Workbench. Language Workbenches are less common but many people think they have the potential to profoundly improve the way we do programming.

DSLs can be implemented either by interpretation or code generation. Interpretation (reading in the DSL script and executing it at run time) is usually easiest, but code-generation is sometimes essential. Usually the generated code is itself a high level language, such as Java or C.

Since we have **Java** as a constraint and external dsls takes a lot a time to make, we are going to tackle this with using internal DSL.

First we need to alter several classes and create new ones to accommodate our internal dsl.

```
/* Map.java */
package mdejava;

import java.util.ArrayList;

public class Map {
	private String title;
	private ArrayList<Topic> rootTopics;

	public ArrayList<Topic> getRootTopics() {
	    return rootTopics;
	}

	void addRootTopic(Topic topic) {
		getRootTopics().add(topic);
	}

	public Map (String title) {
		this.title = title;
		this.rootTopics = new ArrayList<Topic>();
	}

	public static void printMap(Map m){
		System.out.println(" " + m.title);
	    System.out.println("Topics...");
	    for (Topic t : m.getRootTopics()) {
	      System.out.print(t.getName() + " ");
	      System.out.print(t.getDescription() + " ");
	      System.out.print(t.getPriority() + " ");

	    }
	    System.out.println("");
	  }
}

```
```
/* Topic.java */
package mdejava;

import java.util.ArrayList;

public class Topic extends MapElement {

	private String description;
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public Topic getParent() {
		return parent;
	}

	public void setParent(Topic parent) {
		this.parent = parent;
	}

	private String start;
	private String end;
	private Priority priority;
	private Topic parent;

	enum Priority {
		HIGH,
		MEDIUM,
		LOW;
	}

	private ArrayList<Topic> allSubTopics;

	public ArrayList<Topic> getAllSubTopics() {
	    return allSubTopics;
	}

	void addSubTopic(Topic topic) {
		getAllSubTopics().add(topic);
	}

	void removeSubTopic(Topic topic) {
		getAllSubTopics().add(topic);
	}

	public Topic(String name, String description, String start, String end, Priority priority) {

		setName(name);
		this.description = description;
		this.start = start;
		this.end = end;
		this.priority = Priority.HIGH;
		this.allSubTopics = new ArrayList<Topic>();
	}
}

```
Now that we have the Semantic Model in place, lets build the DLSs. Keeping them separate helps in testing the Semantic Model and the DSL independently.
The different approaches for creating Internal DSLs stated by Martin Fowler are:
* Method Chaining
* Functional Sequence
* Nested Functions
* Lambda Expressions/Closures

In this case we are using Method Chaining.
The DSL should look like this.
```
Map()
  .topic()
    .name("some name")
    .description("some description")
    ...
  .printMap;

```
For the creation of such DSL we would have to write an expression builder which allows popuplation of the semantic model and provides a fluent interface enabling creation of the DSL.
For this we create two builder classes, MapBuilder and TopicBuilder.

```
/* MapBuilder.java */
package mdejava;

public class MapBuilder {

	private Map map;

	public MapBuilder() {
	    map = new Map("GorgeousFood");
	  }

	 //Start the Map DSL with this method.
	public static MapBuilder Map(){
	    return new MapBuilder();
	}

	//Start the Topic building with this method.
	public TopicBuilder topic(){
	   TopicBuilder builder = new TopicBuilder(this);

	    getMap().addRootTopic(builder.topic);

	    return builder;
	}

	public Map getMap() {
	    return map;
	}

	public void printMap(){
	    Map.printMap(map);
	}

}

```
```
/* TopicBuilder.java */
package mdejava;

import mdejava.Topic.Priority;

public class TopicBuilder {

	Topic topic;

	MapBuilder mBuilder;

	public TopicBuilder(MapBuilder mBuilder) {
		this.mBuilder = mBuilder;
	    topic = new Topic(null, null, null, null, null);
	}

	public TopicBuilder name(String name){
	    topic.setName(name);
	    return this;
	}
	public TopicBuilder description(String description){
	    topic.setDescription(description);
	    return this;
	}

	public MapBuilder priority(Priority priority){
	    topic.setPriority(priority);
	    return mBuilder;
	}


}
```
Now that we have are builders created we can start testing it.

```
/* MindMapDSL.java */
package mdejava;
import static mdejava.MapBuilder.*;

import mdejava.Topic.Priority;

public class MindMapDSL {

	public static void main(String[] args) {
	     Map()
	      .topic()
	      	.name("Meal Management")
	      	.description("A meal management description")
	      	.priority(Priority.HIGH)
	      .topic()
	      	.name("Item Management")
	      	.description("A item management description")
	      	.priority(Priority.HIGH)
	      .topic()
	      	.name("Ingredient Management")
	      	.description("A ingredient management description")
	      	.priority(Priority.HIGH)
	      .printMap();
	}

}

```
If everything worked as intended we should get the following in the output.
```
GorgeousFood
Topics...
Meal Management A meal management description HIGH Item Management A item management description HIGH Ingredient Management A ingredient management description HIGH
```

### Model to Model Transformations

A model to model transformation is the automatic creation of target models from source models.

A model transformation can be written using some tools such as ATL (Atlas Transformation Language) and QVT (Query/View/Transformation), however a general-purpose programming language like Java also can be used.

To show the usage of Java in a model to model transformation let's consider the next example: it's pretended to transform Maps into ToDoLists. 

A Map contains a list of topics and the objective is to transform each one of the Topics into Tasks. Topics can have other Topics (subtopics), do a Task can also have Tasks (subtasks).

The classes used in this example are the following:

```
public class Map {
	private String title;
	private List<Topic> rootTopics;

	public Map (String title) {
		this.title = title;
		this.rootTopics = new ArrayList<Topic>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Topic> getRootTopics() {
	    return rootTopics;
	}

	public void addRootTopic(Topic topic) {
		this.rootTopics.add(topic);
	}
}
```

```
public class Topic extends MapElement {

	private String description;
	private String start;
	private String end;
	private Priority priority;
	private Topic parent;
	private List<Topic> allSubTopics;

	enum Priority {
		HIGH,
		MEDIUM,
		LOW;
	}

	public Topic(String description, String start,
					String end, Priority priority) {

		this.description = description;
		this.start = start;
		this.end = end;
		this.priority = priority;
		this.allSubTopics = new ArrayList<Topic>();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public Topic getParent() {
		return parent;
	}

	public void setParent(Topic parent) {
		this.parent = parent;
	}

	public List<Topic> getAllSubTopics() {
	    return allSubTopics;
	}

	public void addSubTopic(Topic topic) {
		this.allSubTopics.add(topic);
	}

}
```

```
public class ToDoList {
	private String title;
	private List<Task> tasks;

	public ToDoList () {
		this.tasks = new LinkedList<>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Task> getTasks() {
	    return tasks;
	}

	public void addTask(Task task) {
		this.tasks.add(task);
	}
}
```

```
public class Task {
	private String title;
	private Date start;
	private Date end;
	private List<Task> subTasks;

	public Task () {
		this.tasks = new LinkedList<>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public List<Task> getSubTasks() {
	    return subTasks;
	}

	public void addSubTask(Task task) {
		this.subTasks.add(task);
	}
}
```

The Ecore models related with the previous Java classes are:

Map model
![](./images/mindmap.png)

ToDoList model
![](./images/todolist_model.png)


The Java methods created in order to fulfill the objective are:

```
public ToDoList map2ToDoList(Map map){
	ToDoList toDoList = 
			ToDoListFactory.eINSTANCE.createToDoList();

	toDoList.setTitle(map.getTitle());

	Iterator it = map.getRootTopics().iterator();
	Topic topic = null;

	while (it.hasNext()){
		topic = it.next();
		toDoList.addTask(topic2task(topic));
	}

	return toDoList;
}
```

```
public Task topic2task(Topic topic) {
	Task task = TaskFactory.eINSTANCE.createTask();

	task.setTitle(topic.getDescription());

	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	Date start = dateFormat.parse(topic.getStart());
	Date end = dateFormat.parse(topic.getEnd());

	task.setStart(start);
	task.setEnd(end);

	Iterator it = topic.getAllSubTopics().iterator();
	Topic auxTopic = null;

	while (it.hasNext()){
		auxTopic = it.next();
		task.addSubTask(topic2task(auxTopic));
	}

	return task;
}
```


### Model to/from Text Transformations

The MDE allows the automation of software development through the application of transformations that are applied to the models that describe a system. A particular type of transformations is the Model to Text (M2T) transformations, also known as "code generators". These transformations allow the models to be transformed into source code and documentation.

In order to develop these transformations, there's a normally a set of generation rules and templates that are set and applied to the iterated models. These can either be design decisions or architetural strategies to generate the code, resulting in more maintainable and extensible code.

They can either be done by using different tools like Acceleo, XPand and JET. These tools offer several advantages, easing the process of code generation, by providing template mechanisms that allow the manipulation of metamodels and many more functionalities like model validation. 

However, this research topic focuses on programming language based code generation, to be more specific **Java**. Even though it's harder to develop these transformations by using programming languages, they don't require any tool dependencies or the developers to learn new languages.

To demonstrate it there's a small example below showing the generation of a puml file, in order to document an instance created from the mindmap model.

```
/* GenerateDocs.java */
package mdejava;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class GenerateDocs {

	public static void main(String[] args) {
		loadModel();
	}

	public static void loadModel() {
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("mindmap", new XMIResourceFactoryImpl());

		// Initialize the model
		MindmapPackage.eINSTANCE.eClass();

		// Retrieve the default factory singleton
		MindmapFactory factory = MindmapFactory.eINSTANCE;

		// Obtain a new resource set
		ResourceSet resSet = new ResourceSetImpl();

		Resource resource = resSet.getResource(URI.createURI("instances/exemplo1.mindmap"), true);

		// now load the content.
		PrintWriter writer=null;
		try {
			resource.load(Collections.EMPTY_MAP);

			EObject root = resource.getContents().get(0);
			Map myMap=(Map)root;
			
			System.out.println(root.toString());
			
			FileWriter w = new FileWriter("instances/mindmap.puml");
	        writer = new PrintWriter(w);
	        
	        writer.println("@startuml");
	        writer.println("digraph xpto {");
	        
			// For the Topics
			for (MapElement e: myMap.getElements()) {
				if (e instanceof Topic) {
					Topic t=(Topic)e;
					writer.println(t.getName());
				}
			} 
			
			// For the hierarchy
			for (MapElement e: myMap.getElements()) {
				if (e instanceof Topic) {
					Topic t=(Topic)e;
					for (Topic t2: t.getSubtopics()) {
						writer.println(t.getName()+ " -> "+t2.getName());
					}
				}
			}
			
			//resource.save(Collections.EMPTY_MAP);
			writer.println("}");
	        writer.println("@enduml");		
	        writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			writer.close();
		}
	}
}
```

This is an example mindmap instance, which will be used for documentation generation.

```
<?xml version="1.0" encoding="ASCII"?>
<pt.isep.edom.mindmap:Map xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:pt.isep.edom.mindmap="http://www.example.org/mindmap">
  <elements xsi:type="pt.isep.edom.mindmap:Topic" name="Agenda" subtopics="//@elements.1 //@elements.2"/>
  <elements xsi:type="pt.isep.edom.mindmap:Topic" name="Contacts" parent="//@elements.0"/>
  <elements xsi:type="pt.isep.edom.mindmap:Topic" name="Meeting" parent="//@elements.0"/>
</pt.isep.edom.mindmap:Map>
```

The following puml file was generated from the given mindmap instance. 

```
@startuml
digraph xpto {
Agenda
Contacts
Meeting
Agenda -> Contacts
Agenda -> Meeting
}
@enduml
```

Through this puml file a diagram was generated as we can see in the image below.

![Instance Diagram](./images/diagram.PNG)


## References

### Links
* [Software Development with UML and Modern Java](https://bitbucket.org/blog/software-development-with-uml-and-modern-java)
* [A brief introduction to model-driven engineering](http://www.scielo.org.co/pdf/tecn/v18n40/v18n40a11.pdf)
* [Java Introduction](https://www.w3schools.com/java/java_intro.asp)
* [Modeling in Java](https://www.researchgate.net/publication/278671545_Modelling_in_Java#pfd)
* [Creating Internal DSLs in Java](https://dzone.com/articles/creating-internal-dsls-java)
* [Domain Specific Language](https://martinfowler.com/bliki/DomainSpecificLanguage.html)
* [Model-To-Text Transformations](https://moodle.isep.ipp.pt/pluginfile.php/298887/mod_resource/content/2/edom07_1.pdf)
* [Model-to-Text Transformation Modification by Examples](https://www3.risc.jku.at/publications/download/risc_4555/phd2.pdf)
* [Software Developlement Tools in Model-Driven Engineering](https://www.researchgate.net/publication/320756996_Software_Development_Tools_in_Model-Driven_Engineering)