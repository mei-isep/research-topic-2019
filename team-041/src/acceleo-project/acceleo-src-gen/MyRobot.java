public class MyRobot {

	public MyRobot() {
	}
	
	public void goForward(int length){
		System.out.println("Going forward: " + length + " cm");
	}
	
	public void grab(){
		System.out.println("Grabing!");
	}
	
	public void rotate(int degrees){
		if (degrees > 0){
			System.out.println("Rotating left: " + degrees + " degrees");
		} else {
			System.out.println("Rotating right: " + degrees + " degrees");
		}
	}
	
	public void release(){
		System.out.println("Releasing!");
	}
}
