/**
 */
package pt.isep.edom.mindstorms.impl;

import org.eclipse.emf.ecore.EClass;

import pt.isep.edom.mindstorms.Action;
import pt.isep.edom.mindstorms.MindstormsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ActionImpl extends InstructionImpl implements Action {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MindstormsPackage.Literals.ACTION;
	}

} //ActionImpl
