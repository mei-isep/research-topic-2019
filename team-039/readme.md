# EDOM - Project Research Topic - TEXTUML



## UML

UML (Unified Modeling Language) é uma linguagem de simples compreensão, que auxilia equipes de produção de software a ter maior eficiência e eficácia no dia a dia, possibilitando uma comunicação clara e objetiva sobre o que deve ser feito, e como deve ser feito. É expressa através de diagramas, compostos por elementos (formas gráficas usadas para os desenhos) que possuem relação entre si. Dividindo-se em dois grandes grupos: diagramas estruturais e diagramas comportamentais.

- **Diagramas estruturais** : são utilizados para especificar detalhes da estrutura do sistema como, por exemplo, classes, métodos, interfaces, namespaces, serviços, como componentes devem ser instalados ou até como deverá ser a arquitetura do sistema.

- **Diagramas comportamentais** : são utilizados para especificar detalhes do comportamento do sistema como, por exemplom, as funcionalidades devem funcionar, um processo de negócio deve ser tratado pelo sistema e componentes estruturais trocam mensagens.


![MarketPlace](images/UML.png)
      Figura1 - UML 



## TextUML

TextUML é um plugin open-source para diagram UML que permite a criação de modelos ao mesmo tempo que se escreve código. Pode ser usado com **_Cloudfier_** (aplicação _web-browser_) ou como _plugin_ de eclipse: **TextUML Toolkit**. É uma ferramenta que atua como uma linha de comandos independente/editor de texto e como parte de aplicação servidor de multiutilizadores (_Cloudfier_). 
 

O foco principal desta _framewrok_ é suporte a MDD(model-driven development) auxiliando a criação de modelos UML que tipicamente aparecem em contextos de diagramas de classes (Figura3), diagramas de atividades e diagramas de estados. Segundo o criador, o motivo do seu desenvolvimento era a **orientação a modelos** e os diagramas descritos são os que melhor caracterizam este contexto. 

## TextUml Toolkit

O TextUML também pode ser encontrado como um plugin no marketplace do Eclipse - tendo o nome de TextUML Toolkit. Este plugin tem compatibilidade com outros _frameworks_ do eclipse como UML2 e EMF, permitindo, assim, uma criação de metamodelos de UML 

![MarketPlace](images/MPeclipse.PNG)
      Figura2 - MarketPlace Eclipse


###  Ferramentas textuais de UML

Uma ferramenta textual UML suporta o uso de notações textuais que descrevem modelos UML e que automaticamente renderiza diagramas UML através desse texto. É um dos mercados que mais rápido cresceu no mundo das ferramentas UML. Com ajuda de _online modeling tools_ é possível facultar uma opção mais fácil para desenho de modelos UML.  

São bastante famosos devido ao facto de não existir qualquer restrição para usufruir delas. Os modelos UML ao serem guardados como texto simplifica bastante a integração com uma variedade de ferramentas que os programadores já utilizam no seu dia a dia. Sendo assim, não é necessário a aprendizagem de ferramentas adicionais. Para não falar que os desenvolvedores sentem um conforto muito maior com linguagens textuais - algo que usam com bastante frequencia: escrita de código, do que com linguagens gráficas (até porque "o foco principal são os _developers_, não _business analysts_", disse Rafael Chaves- criador do TextUML). Os modelos gráficos atraem pessoas que trabalham com altos níveis de abstração, mas para desenvolvedores muito mais natural usar uma notação textual.
 
 Outra ferramenta bastante conhecida é o PlantText UML(PUML), suportada por um plugin do **VISUAL STUDIO CODE**.



                   
![TextUML toolkit](images/textuml.png)

      Figura3 -TextUML toolkit

        
![PUML no VSCode](images/puml.PNG)

      Figura4 - PlantText UML + Visual Studio CODE
      
#### TextUML vs Outras Ferramentas de Notação textual de UML

A ferramenta PlantUML(PUML), a ferramenta mais conhecida nesta categoria, fornece suporte para os diagramas de UML "mais importantes". Para além disto, outro ponto forte desta ferramenta em relação a qualquer outra de notação de UML textual é o facto de existir um enorme "ecossistema" de plataformas que permitem a renderização dos diagramas UML.
Contudo, o TextUML também tem uma enorme vantagem, permite a geração do código com a **integração de uma outra ferramenta: Acceleo**.
Além do mais, a sua notação textual é bastante parecida com linguagens de programação muito comuns como JAVA E C#.
Segundo o criador do TextUML, a sua ferramenta é bastante diferente de outras ferramentas de UML (PUML,yUML), pois estas geram diagramas de UML, e a sua gera modelos de UML - gerar diagramas gráficos era só uma função adicional.


### Integração do Acceleo 

O Acceleo é uma ferramenta _open-source_ que gera código apartir de metamodelos de base EMF. A maior parte dos seus componentes são compatíveis com modelos UML2. Isto significa que pode ser usado o TextUML Toolkit para uma rápida criação de modelos UML usando uma notação textual e usar o Acceleo para gerar o código a partir destes modelos.  

### Conclusão

Hoje em dia é cada vez mais comum programadores
recorrerem a notações textuais de linguagem UML,
devido a familiaridade e facilidade que estas
fornecem-lhes, para a modelação e vistas de
projetos que iram desenvolver.

O TextUML é uma das ferramentas mais conhecidas
pelos os desenvolvedores de software. A sua
notação bastante parecida com linguagens de
escrita de código dá bastante conforto aos seus
utilizadores, pois não têm a necessidade de
estudar uma nova lógica de "programação".

Além do mais, fornece uma variedade de compatibilidade com outros plugins, que facilita quer a geração de esqueletos de código a código base 100% funcional. O que leva a que esta _framework_ seja uma mais valia no início de qualquer projeto.  


### Referências

https://pt.slideshare.net/rachaves/vijug20080326

https://modeling-languages.com/text-uml-tools-complete-list/


http://abstratt.github.io/textuml/readme.html

https://wiki.eclipse.org/MDT/UML2

https://modeling-languages.com/coffee-rafael-chaves-textuml-tool/

https://www.ateomomento.com.br/diagramas-uml/

https://pt.wikipedia.org/wiki/UML

