# EDOM - Project Research Topic - Graphical Concrete Syntaxes (Using Microsoft DSL SDK)

December 2019

## Contents

1. [Introduction](#introduction)
2. [Graphical Concrete Syntaxes](#graphical-concrete-syntaxes)
3. [Modeling SDK for Visual Studio](#modeling-sdk-for-visual-studio)
4. [Compare Xtext with Microsoft Modeling SDK](#compare-xtext-with-microsoft-modeling-sdk)

## Introduction 

This research was carried out within the _Domain Engineering_ (EDOM) course on the _Software Engineering Minor_ of the _Masters in Informatics Engineering_ (MEI) of _Porto's School of Engineering_ (ISEP), aiming to acquire knowledge about different tools for each one of the course's components.

For research, the group focused on Microsoft's Modeling SDK, a framework to develop concrete graphical syntaxes on Visual Studio and how it works.

This document will give a theoretical introduction of what is a graphical concrete syntax, will explain briefly how Microsoft's tool works and will compare Microsoft's DSL framework with the equivalent tool we use course unit - Eclipse's Xtext.

## Graphical Concrete Syntaxes

Any Domain-Specfic Language (DSL) is composed by two main components: a syntax (composed by its abstract and concrete parts) and the semantics (compose by the domain and and the mappings). 

 | ![figure0](./images/domain-syntax-relation.png) |
 | :--: |
 | Figure 0 - Relationship between the domain and the language's syntaxes, Source: [\[1\]](#references) |

The syntax is specified in two parts: 
* the abstract syntax which defines the language's concepts and their allowed combinations (for example, how an entity should relate to another);
* the concrete syntax defines how those concepts are presented to the user (for example, how an entity should be represented in that language).
 
The concrete syntax of a language can be either textual or graphical and is what a designers look up as a reference in modeling activities.

The graphical concrete syntax is defined by the structure of the abstract syntax and a set of graphical representations for classes and associations in the abstract syntax.

Howevever if the language does not have an adequate graphical representation, a textual syntax  can also be used for modeling which is usually described by a context-free grammar, meaning that modeling activies are not limited only to graphical languages. [\[0\]](#reference)

## Modeling SDK for Visual Studio

With Visual Studio, it's possible to create powerful model-based development tools, using Modeling SDK. This tool surround the model with a diagram Views, the ability to generate code, commands for transforming the model and the hability to interact with code and other objects in Visual Studio [2], it's represented on figure 1.

 | ![figure1](./images/familyt_instance.png) |
 | :--: |
 | Figure 1 - View of MSDK for Visual Studio, Source: Source: [\[3\]](#references) |


The MSDK (Modeling Software Development Kit) allows to develop a model quickly in the form of a DSL (Domain-Specific Language) [2].

To define a DSL, the following components must be installed: Visual Studio Visual Studio SDK Modeling SDK ( In Visual Studio 2019, the component is automatically installed as part of the Visual Studio extension devlopment) Text Template Transformation (Also, in Visual Studio 2019, is installed as part of the Visual Studio extension Devlopment)

### Create a DSL Solution

To create a new Domain-Specific language, first it's required to use a Visual Studio Solution, using a Domain-Specific Language Project Template.

In order to create a new one, the steps should be:

1.On the File menu, point to New, and then click Project.  
2.Under Project types, search for Domain-Specific Language 

| ![Figure 2](./images/DSLPhoto.PNG) | 
|:--:| 
| Figure 2 - Create a DSL Solution, Source:[\[3\]](#references) |

3.Click Domain-Specific Language Designer.  
4.Insert the name and change location (optional choice) and confirm.

| ![Figure 3](./images/DSLPhoto2.PNG) | 
|:--:| 
| Figure 3 - Create a DSL Solution - Part 2, Source:[\[3\]](#references) |

5.Choose MinimalLanguage and confirm to create a DSL Solution

| ![Figure 4](./images/DSLPhoto2.PNG) | 
|:--:| 
| Figure 4 - Create a DSL Solution - Part 3, Source:[\[3\]](#references) |

The Domain-Specific Language Wizard opens, and displays a list of template DSL solutions.

### The important parts of the DSL Solution

* DSL\DslDefinition.dsl : All the elements and relationships of the model are define in this file, on a diagram. Almost all the code in the solution are generated from this file. 
* DSL Project : This project contains code that defines the Domain-Specific Language.
* DslPackage project: this project contains code that allows instances of the DSL to be opened and edited in Visual Studio.

### Running the DSL 

1. In the solution toolbar, click  Transform All Templates. This funcionality regerates most of the source code from DslDefinition.dsl  
2. Press F5 to start Debugging or on Debug menu, click Start Debugging.  
    The DSL builds and is installed in the experimental instance of Visual Studio.  
3. In the experimental instance of Visual Studio, open the model file named Test from Solution Explorer.  
4. Use the tools to create shapes and connectors on the diagram.  

| ![Figure 3](./images/dsl.png) | 
|:--:| 
| Figure 3 - Running DSL, Source: [\[3\]](#references) |

## Compare Xtext with Microsoft Modeling SDK

The modeling SDK tool from Microsoft is instinctively compared to the creation of graphical class diagrams. During the course of EDOM, Xtext was used to implement textual concrete syntaxes that leads to a framework capable of building language workbenches for textual domain-specific languages.

Comparing both at first sight, it's possible to fell that microsoft modeling SDK framework can be more user friendly than Xtext framework [\[3\]](#references).

![Figure N](./images/domain_model_xtext.png)
Figure 4 - Example of a DSL, Source: https://www.eclipse.org/Xtext/documentation/102_domainmodelwalkthrough.html

Ecore model representing the abstract syntax tree is instantiated after analysis from a parser to the text files created by the Xtext editor. Using Xtext brings the advantage of integrate it into different IDE's (IntelliJ, Eclipse, VS Code, Atom and others), not depending on only one technology [\[4\]](#references). On the other hand, using Modeling SDK, the generated files only can be opened on Visual Studio.

A DSL implemented in Xtext can also be implemented in Xtend. Xtend is implemented in Xtext and is more user friendly. It allow to write a better readable code. In Modeling SDK, template files (.tt) are generated with notations to process the DSL. This one are implemented in C#. 

Regarding the maintainability of the models, it is easier to create and maintain models via text files, as it is the case of Xtext. However it is beneficial to have a graphical representation to discuss the broader domain concepts and their relations, like Microsoft DSL SDK.

Domain-specific languages are used to express concepts of a certain domain in a concise and semantically rich notation. Making use of DSLs enables model simulation, source code generation, and increases quality. Xtext is a framework built to create domain-specific languages including a very good and fast editor. Also, Xtext is very customizable supporting the evolution of a DSL from the prototype stage version into a more mature solution. It provides mature features for model-to-model or model-to-text transformations. With the combination of Xtext and Xtend it becames easy and fast to create a domain-specific language workbench in the perfect measure. [\[4\]](#references)

## References

[0]: Sintef.no. (2018). Model-Driven Analysis and Synthesis of Textual Concrete Syntax. [online] Available at: https://www.sintef.no/globalassets/upload/ikt/9012/mdasocs-sosym.pdf.

[1]: S. Kelly, "Concrete syntax matters", 2012. Retrieved from https://www.slideshare.net/stevekmcc/concrete-syntax-matters

[2]: Contributors (2016, April 11). Modeling SDK for Visual Studio - Domain-Specific Languages Retrived from https://docs.microsoft.com/en-us/visualstudio/modeling/modeling-sdk-for-visual-studio-domain-specific-languages?view=vs-2019

[3]: Contributors (2016, April 11). Get Started with Domain-Specific Languages, Retrived from https://docs.microsoft.com/en-us/visualstudio/modeling/getting-started-with-domain-specific-languages?view=vs-2019

[4]: Bunder, Hendrik (2018, Mar 15). Building Domain-specific Languages with Xtext and Xtend. Retrieved from https://blogs.itemis.com/en/building-domain-specific-languages-with-xtext-and-xtend?fbclid=IwAR1KTR4vNPtffL-uJELwIgfs88SvyKQBY1Ed4F0iiEY7GnHWR8hXcUWDsS8

[5]: Eclipse (2013, December 16), Xtext 2.5 Documentation. Retrieved from https://www.eclipse.org/Xtext/documentation/index.html