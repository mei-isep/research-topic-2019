# EDOM - Tópico de Pesquisa ESON

## Introdução

No âmbito da unidade curricular de EDOM (Engenharia de Domínio), foi proposto a todos os alunos que realizassem uma pesquisa sobre uma tecnologia ou ferramenta de apoio relacionadas com os temas abordados ao longo do semestre. (Metamodeling, DSL, transformações modelo para modelo e Modelo para código).
Optou-se por elaborar uma pesquisa sobre uma ferramenta implementada com base no Xtext, que basicamente é uma sintaxe textual para modelos EMF, designada como ESON. Em capítulos posteriores pode encontrar-se uma descrição mais detalhada desta ferramenta.
Neste relatório, numa primeira fase, encontra-se descrito detalhadamente o que é metamodeling, com o intuito de obter um melhor enquadramento à cerca do tema de pesquisa.
Seguidamente iremos ter uma secção destinada para perceber melhor a tecnologia abordada (ESON) onde se encontram uma série de exemplos ilustrativos, com o intuito de perceber o que a tecnologia ESON faz.
Também podemos encontrar uma secção, onde será apresentado um cenário real onde esta tecnologia pode ser utilizada e como é que esta é aplicada, explicando pormenorizadamente como esta é utilizada.
Em seguida são apresentadas as alternativas ao uso do ESON, explicando de maneira sucinta de como é que estas alternativas funcionam.
Por fim é apresentada uma breve conclusão, onde é realizada uma análise do trabalho desenvolvido. É também, nesta secção onde se encontra uma breve apreciação final do trabalho realizado.

## Metamodeling

Um metamodelo ou modelo substituto, basicamente funciona como um modelo de um modelo, isto é, um modelo simplificado de um modelo real de um sistema ou software como entidade. Metamodeling é o processo utilizado para criar esses metamodelos. Deste modo podemos afirmar que o metamodeling é a análise, construção e desenvolvimento dos quadros, regras, restrições, modelos e teorias úteis e aplicáveis para modelar uma classe predefinida de problemas. Como o próprio nome indica, este conceito implica noção de meta e modelagem em engenharia de software.
A especificação atual do UML oferece um suporte ao processo de metamodeling para estender os recursos de linguagens e adaptá-los a domínios de modelagem específicos. As ferramentas de metamodeling visam acelerar o desenvolvimento de sistemas, uma vez que os conceitos de metamodeling podem ser vinculados ao código do programa parametrizado.

## ESON

A ferramenta ESON (EMF *Simple Object Notation*) foi implementada com *Xtext* e fornece uma sintaxe textual, baseada numa notação JSON, usada para criar instâncias dinâmicas de modelos *Xcore*. Foi criada em 2010 com o nome *EFactory* e posteriormente mudou de nome para ESON. Hoje em dia, o projeto do ESON esta incluído na *Eclipse Modeling Framework* (EMF) como um subprojecto desde Dezembro de 2014.

### Sintaxe JSON

​	Como a sintaxe do ESON é baseada no JSON, para criar uma nova EClass só é preciso declarar o seu nome e  introduzir os seus elementos entre chavetas. Para relações entre classes de um para muitos é usado os parenteses retos para declarar a introdução de vários elementos, como podemos observar nas entre as linhas 28 e 40 aonde são criados vários livros.

![](Images/Example1.png)

### Referencias

​	Ao criar instâncias de metamodelos é possível referenciar os objetos criados noutros objetos através do seu *EAttribute name* ou usando outro *EAttribute* qualquer desde que seja declarado usando a anotação `  @Name { Book = title } `. No exemplo acima é possível observar que a autora "J.K.Rowling" esta a ser referenciada pelo livro Harry Potter através no nome ` lib1.J.K.Rowling `.

### Interface dinâmica

​	Durante a criação de instâncias dinâmicas usando a UI do ESON temos duas opções de edição. Do lado esquerdo usamos a sintaxe do ESON como foi apresentado anteriormente e do lado direito temos uma visão de árvore do modelo aonde também se pode adicionar novos elementos. Ambas as vistas são sincronizadas dinamicamente portanto alterações em um dos lados é refletida no outro. Além destas duas vistas também é possível utilizar a janela *Properties* para editar o modelo.

![](Images/TreeView.png)



### Funcionalidades Quality of Life

​	Ao editar o modelo usando o ESON a ferramenta está constantemente a verificar se existem erros de sintaxe, nos nomes das classes/atributos e nas referências a outros objetos. Quando o sistema encontra um erro este é realçado a com uma linha vermelha e uma mensagem de erro.

​	Para manter a boa legibilidade do texto que representa o modelo o ESON inclui um formatador de texto que pode ser ativado com a combinação de teclas `  Ctrl-Shift-F `. Além disto, também é disponibilizado uma funcionalidade de *autocomplete* que nos indica as alternativas possíveis no local atual de edição usando a combinação `Ctrl-Space `. É comum as ferramentas desenvolvidas com o *Xtext* possuírem a funcionalidade de *autocomplete* pois esta é ativada automaticamente.

## Cenário

De forma a compreeender a usabilidade da ferramenta em estudo, passamos a apresentar um possível cenário real aonde o uso do ESON traz valor a um utilizador.

Partindo do exemplo de uma biblioteca, pretendemos desenvolver um sistema que permita a gestão de diversos livros. Acerca destes, interessam-nos guardar diversos dados, como por exemplo: título, autor, número de páginas e género literário.

Para este fim, a organização adota os princípios de MDE (Model Driven Engineering), de forma reduzir os tempos de implementação e manutenção deste programa, uma vez que dependerá principalmente de operações **CRUD**. Seguindo esta prática, começamos pela definição de um metamodelo que servirá para instanciar modelos apropriados ao nosso caso de uso. Para o nosso caso de uso específico, interessam-nos representar as classes 'Book', 'Author', 'Writer' e 'BookCategory'.

De seguida, passamos à fase de instanciação, aonde criamos um modelo baseado no metamodelo previamente definido. Neste passo, interessa-nos armazenar a informação acerca dos livros que o nosso sistema vai gerir. Recorrendo ao ESON e à janela de pré-visualização incluída com a ferramenta, podemos facilmente editar o nosso modelo usando um editor de texto, obtendo feedback em tempo real. Graças à fácil legibilidade da sintaxe adotada no ESON, reduzimos o tempo necessário para modelar a informação que pretendemos gerir e armazenar. Tendo em conta o caso de uso de uma biblioteca, aonde é importante considerar a possível adição/remoção de novos livros no sistema, a utilização de ESON agiliza este processo.

Assim que temos o nosso modelo criado, passamos à sua (possível) refinação, terminando com uma transformação **model-to-text** que vai produzir o código-fonte através do qual executamos o programa que nos permite gerir os livros da nossa biblioteca.

### Alternativas

Como alternativa ao uso de ESON, existem ferramentas como XMI (XML Metadata Interchange) ou HUTN (Human Usable Textual Notation) que permitem a um utilizador instanciar modelos dinâmicos.

Incluído com a EMF (Eclipse Modelling Framework), temos o XMI. Partindo de um metamodelo descrito num ficheiro do tipo Ecore, podemos criar uma instância dinâmica com um formato semelhante ao de um ficheiro XML. Isto implica que, sem recorrer a uma interface de edição, a edição via texto é bastante complicada devido à baixa legibilidade.

Comparativamente ao ESON, o XMI tem a vantagem de já estar incluído na EMF; por outro lado, uma vez que o ESON baseia a sua sintaxe no formato JSON, o que produz um resultado mais legível.

Porém, o HUTN também tem como principal preocupação o armazenamento de modelos num formato facilmente legível. Para este fim, recorre ao uso de uma sintaxe semelhante à de JSON, recorrendo ao uso de chavetas ao contrário das tags normalmente associadas ao formato XML.

## Conclusão

Como podemos concluir o ESON é uma ferramenta implementada com o *Xtext*, no qual a sua sintaxe textual é baseada em JSON. Esta ferramenta é utilizada para criar instâncias dinâmicas de modelos *Xcore*.
Podemos também concluir, que esta ferramenta não é muito utilizada uma vez que esta não é muito acessível. Numa opinião pessoal do grupo, uma boa alternativa ao uso do ESON seria o *Xmi*, visto que este fora abordado nas aulas ao longo do semestre e pareceu-nos mais fácil de se utilizar. Para além disso existem ferramentas de edição de ficheiros *Xmi* nos packages oficiais do eclipse.

## Bibliografia

* https://en.wikipedia.org/wiki/Metamodeling
*  [https://www.eclipse.org/modeling/emf/](https://www.eclipse.org/modeling/emf/?fbclid=IwAR1AXt-KK_dZP1cODRzATCkQSq0ez-1N8nY_UOioCnHXBFa9LtHQk0gAfZs)
*  https://wiki.eclipse.org/ESON 
*  https://git.eclipse.org/c/emf/org.eclipse.emf.eson.git/ 

